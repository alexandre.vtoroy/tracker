
#include <taskdelegate.h>
#include <QPainter>
#include <QTextDocument>

TaskDelegate::TaskDelegate(QObject* parent)
    : QStyledItemDelegate(parent)
{
}

void TaskDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    painter->setRenderHint(QPainter::Antialiasing, true);
    painter->setRenderHint(QPainter::TextAntialiasing, true);

    auto options = option;
    initStyleOption(&options, index);

    painter->save();

    const int d = 1;
    QRectF rectBound = option.rect.adjusted(d, d, -d, -d);
    QVariant backgroundValue = index.data(Qt::BackgroundRole);
    QColor background = backgroundValue.isValid() ? backgroundValue.value<QColor>() : QColor(Qt::white);
    painter->setBrush(background);
    painter->drawRoundedRect(rectBound, 4*d, 4*d);
    painter->restore();

    painter->save();

    QTextDocument doc;
    QTextOption textOption(doc.defaultTextOption());
    textOption.setWrapMode(QTextOption::WordWrap);
    doc.setDefaultTextOption(textOption);
    doc.setHtml(options.text);
    doc.setTextWidth(options.rect.width());

    //options.text = "";
    //options.widget->style()->drawControl(QStyle::CE_ItemViewItem, &option, painter);
    painter->translate(options.rect.left(), options.rect.top());

    QRect clip(0, 0, options.rect.width(), options.rect.height());
    doc.drawContents(painter, clip);

    painter->restore();
}

QSize TaskDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    initStyleOption(const_cast<QStyleOptionViewItem*>(&option), index);

    QTextDocument doc;
    QTextOption textOption(doc.defaultTextOption());
    textOption.setWrapMode(QTextOption::WordWrap);
    doc.setDefaultTextOption(textOption);
    doc.setHtml(option.text);
    doc.setTextWidth(option.rect.width());
    return QSize(doc.idealWidth(), doc.size().height());
}
