
#include <utils.h>
#include <QColor>
#include <qglobal.h>
#include <QRandomGenerator>
#include <QTime>

QColor Utils::randomColor()
{
    static bool isInit;
    static QRandomGenerator random;
    if(!isInit)
    {
        QTime time = QTime::currentTime();
        random.seed((uint)time.msec());
        isInit = true;
    }
    int hue = random.bounded(359);
    return QColor::fromHsl(hue, 128, 200);
}

QStringList Utils::sqlFieldsNames(const QStringList& defines)
{
    QStringList result;
    for(const QString& define: defines)
    {
        QStringList parts = define.split(" ");
        QString name = parts[0];
        result.append(name);
    }
    return result;
}

QStringList Utils::addPrefix(const QStringList& items, const QString& prefix)
{
    QStringList result;
    for(const QString& item: items)
        result.append(prefix + item);
    return result;
}

QDateTime Utils::parseDateTime(const QString& str)
{
    QStringList parts = str.split("T");
    QStringList dparts = parts[0].split("-");
    int year = dparts[0].toInt();
    int month = dparts[1].toInt();
    int day = dparts[2].toInt();

    QDate date(year, month, day);
    QTime time = QTime::fromString(parts[1]);

    return QDateTime(date, time);
}

QString Utils::stdToString(const QDateTime& dateTime)
{
    QString date = dateTime.toString("ddd d-MMMM-yyyy");
    QString time = dateTime.toString("HH:mm");
    return date + " at " + time;
}

