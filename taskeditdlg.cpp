
#include <QComboBox>
#include <QDateTime>
#include <QDebug>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QTextEdit>
#include <colorwidget.h>
#include <settings.h>
#include <taskeditdlg.h>
#include <tasksmodel.h>
#include <utils.h>

TaskEditDlg::TaskEditDlg(TasksModel* tasks, QWidget* parent)
    : QDialog(parent), myTasks(tasks)
{
    setFont(Settings::commonFont());
}

void TaskEditDlg::buildLayout(const QStringList& fields, const QList<QVariant>& values)
{
    myFields = fields;
    myWidgets.clear();

    QGridLayout* layout = new QGridLayout(this);
    int n = fields.size();
    for(int i=0; i<n; i++)
    {
        QWidget* widget = createWidget(fields[i], values[i]);
        myWidgets.append(widget);
        layout->addWidget(new QLabel(fields[i]), i, 0);
        layout->addWidget(widget, i, 1);
    }
    QHBoxLayout* buttons = new QHBoxLayout(this);
    QPushButton* ok = new QPushButton(tr("OK"), this);
    QPushButton* cancel = new QPushButton(tr("Cancel"), this);
    buttons->addWidget(ok);
    buttons->addWidget(cancel);
    buttons->addStretch(1);
    connect(ok, &QPushButton::clicked, [=]{ this->accept(); });
    connect(cancel, &QPushButton::clicked, [=]{ this->reject(); });

    layout->addLayout(buttons, n+1, 0, 1, 2);
    layout->setColumnStretch(0, 0);
    layout->setColumnStretch(1, 1);
    layout->setRowStretch(n, 1);
}

QWidget* TaskEditDlg::createWidget(const QString& field, const QVariant& value) const
{
    bool isDisabled = field=="id" || field=="updated";
    QString valueStr = value.toString();
    QWidget* result;
    QWidget* thisWidget = const_cast<TaskEditDlg*>(this);
    if(field=="comments")
    {
        QTextEdit* edit = new QTextEdit(thisWidget);
        edit->setText(valueStr);
        edit->setAcceptRichText(false);
        result = edit;
    }
    else if(valueStr.startsWith("#"))
    {
        ColorWidget* color = new ColorWidget(thisWidget);
        color->setColor(valueStr);
        color->setFixedSize(100, 50);
        result = color;
    }
    else if(field=="state")
    {
        QComboBox* combo = new QComboBox(thisWidget);
        QList<int> ids = myTasks->states();
        QStringList names = myTasks->statesNames();
        int currentStateId = valueStr.toInt();
        int currentStateIndex = ids.indexOf(currentStateId);
        for(int i=0, n=ids.size(); i<n; i++)
            combo->addItem(names[i], ids[i]);
        combo->setCurrentIndex(currentStateIndex);
        result = combo;
    }
    else
    {
        QLineEdit* line = new QLineEdit(thisWidget);
        if(field=="updated")
        {
            QDateTime dateTime = Utils::parseDateTime(valueStr);
            valueStr = Utils::stdToString(dateTime);
        }
        line->setText(valueStr);
        result = line;
    }
    result->setEnabled(!isDisabled);
    return result;
}

QList<QVariant> TaskEditDlg::currentValues() const
{
    QList<QVariant> values;
    for(int i=0, n=myFields.size(); i<n; i++)
        values.append(currentValue(myFields[i], myWidgets[i]));
    return values;
}

QVariant TaskEditDlg::currentValue(const QString&, QWidget* widget) const
{
    QTextEdit* edit = dynamic_cast<QTextEdit*>(widget);
    if(edit)
        return edit->toPlainText();

    ColorWidget* color = dynamic_cast<ColorWidget*>(widget);
    if(color)
        return color->color().name();

    QLineEdit* line = dynamic_cast<QLineEdit*>(widget);
    if(line)
        return line->text();

    QComboBox* combo = dynamic_cast<QComboBox*>(widget);
    if(combo)
        return combo->currentData();

    return QString();
}
