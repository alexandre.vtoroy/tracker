
#pragma once

#include <QObject>
#include <QSqlDatabase>

class QAbstractTableModel;
class QSqlQueryModel;

class DbEngine : public QObject
{
public:
    DbEngine(QObject* parent = nullptr);
    virtual ~DbEngine() override = default;

    QAbstractTableModel* stateModel() const;
    QAbstractTableModel* createTaskModel(const QString& selector, bool onlyNumber) const;
    void                 resetTaskModel(QAbstractTableModel*) const;

    void createNewTasks(const QStringList& rawLines);
    void changeState(int id, int state);
    QList<QVariant> taskValues(int id, QStringList& fields) const;
    void            updateTask(int id, const QList<QVariant>& newValues);

    QStringList findTasksDoneBetween(const QDateTime& start, const QDateTime& end) const;

private:
    void createTable(const QString& table, const QStringList& defines, int indices);
    QSqlQueryModel* createModel(const QString& table, const QStringList& fields,
                                const QString& selector, const QString& orderBy,
                                bool onlyNumber) const;

    void addState(const int id, const QString& name, const QColor& color);

private:
    QSqlDatabase myDatabase;
    QSqlQueryModel* myStateModel;
};
