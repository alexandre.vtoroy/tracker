
QT       += core gui sql
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17
VERSION = 1.1.1
DEFINES += VERSION_STRING=\\\"$${VERSION}\\\"

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

DISTFILES += \
    ISSUES \
    print_tasks.py \

SOURCES += \
    clipengine.cpp \
    colorwidget.cpp \
    dbengine.cpp \
    filtermodel.cpp \
    filterpane.cpp \
    flowlayout.cpp \
    main.cpp \
    mainwindow.cpp \
    notespane.cpp \
    settings.cpp \
    snippetsboard.cpp \
    taskdelegate.cpp \
    taskeditdlg.cpp \
    tasksboard.cpp \
    tasksheaderview.cpp \
    tasksmodel.cpp \
    utils.cpp

HEADERS += \
    clipengine.h \
    colorwidget.h \
    dbconfig.h \
    dbengine.h \
    filtermodel.h \
    filterpane.h \
    flowlayout.h \
    mainwindow.h \
    notespane.h \
    settings.h \
    snippetsboard.h \
    taskdelegate.h \
    taskeditdlg.h \
    tasksboard.h \
    tasksheaderview.h \
    tasksmodel.h \
    utils.h

TARGET = ztracker.$${VERSION}

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
