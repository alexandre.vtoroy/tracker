
import sqlite3

conn = sqlite3.connect('../build-tracker-Desktop_Qt_5_15_0_MinGW_64_bit-Release/tasks.sqlite')
cur = conn.cursor()

def get(table):
    cur.execute(f"SELECT * FROM {table}")
    for rec in cur.fetchall():
        print(rec)

print("States:")
get("states")
print("Tasks:")
get("tasks")
