
#pragma once

#include <QTableView>

class FilterModel;
class TasksModel;

class TasksBoard : public QTableView
{
    Q_OBJECT
public:
    TasksBoard(TasksModel* tasksModel, FilterModel* filterModel, QWidget* parent = nullptr);
    virtual ~TasksBoard() override = default;

public slots:
    void onEdit(const QModelIndex&) const;

private:
    FilterModel* myFilter;
    TasksModel* myTasks;
};
