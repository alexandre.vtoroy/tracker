
#pragma once

#include <QWidget>

class QPushButton;
class QVariantAnimation;

class SnippetsBoard : public QWidget
{
    Q_OBJECT
public:
    SnippetsBoard(QWidget *parent = nullptr);
    virtual ~SnippetsBoard() = default;

signals:
    void monthlyReport();

private slots:
    void onCopyWithAnimation();
    void onAnimation(const QVariant& value);

private:
    QVariantAnimation *myAnimation, *myBackAnimation;
    QPushButton*       myCurrent;
};
