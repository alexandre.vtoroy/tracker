
#include <QCheckBox>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <filtermodel.h>
#include <filterpane.h>

FilterPane::FilterPane(QWidget* parent)
    : QDockWidget(parent), myModel(new FilterModel(this))
{
    setWindowTitle(tr("Filters"));

    QWidget* filters = new QWidget(this);
    setWidget(filters);

    QGridLayout* layout = new QGridLayout(filters);
    QLabel* labPattern = new QLabel(tr("Search:"), filters);
    myPattern = new QLineEdit(filters);
    mySearchInComments = new QCheckBox(tr("Search in comments:"));
    QPushButton* search = new QPushButton(tr("Search"));

    layout->addWidget(labPattern, 0, 0);
    layout->addWidget(myPattern, 0, 1);
    layout->addWidget(mySearchInComments, 0, 2);
    layout->addWidget(search, 0, 3);

    layout->setColumnStretch(0, 0);
    layout->setColumnStretch(1, 1);

    connect(search, &QPushButton::clicked, this, &FilterPane::onSearch);
    connect(myPattern, &QLineEdit::returnPressed, this, &FilterPane::onSearch);
    connect(mySearchInComments, &QCheckBox::clicked, this, &FilterPane::onSearch);
}

FilterModel* FilterPane::model() const
{
    return myModel;
}

void FilterPane::onSearch()
{
    myModel->setPattern(myPattern->text(), mySearchInComments->isChecked());
}
