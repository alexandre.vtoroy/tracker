
#include <QDebug>
#include <filtermodel.h>
#include <tasksmodel.h>

FilterModel::FilterModel(QObject* parent)
    : QAbstractProxyModel(parent)
{
}

bool FilterModel::filterAccept(const QModelIndex& source) const
{
    QString description = source.data(DescriptionRole).toString().toLower();
    bool matched = description.contains(myPattern);
    if (!matched && mySearchInComments)
    {
        QString comments = source.data(CommentsRole).toString().toLower();
        matched = comments.contains(myPattern);
    }
    return matched;
}

void FilterModel::setPattern(const QString& pattern, bool searchInComments)
{
    beginResetModel();

    myData.clear();
    myPattern = pattern.toLower();
    mySearchInComments = searchInComments;
    myColCount = sourceModel()->columnCount();
    myRowCount = 0;
    int rows = sourceModel()->rowCount();

    for(int j=0; j<myColCount; j++)
    {
        ColumnData cd;
        for(int i=0; i<rows; i++)
        {
            QModelIndex source = sourceModel()->index(i, j);
            if (filterAccept(source))
                cd.append(i);
        }
        myRowCount = qMax(myRowCount, cd.size());
        myData.append(cd);
    }
    myRowCount++;
    endResetModel();
}

QModelIndex FilterModel::index(int row, int column, const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return createIndex(row, column);
}

QModelIndex FilterModel::parent(const QModelIndex &child) const
{
    Q_UNUSED(child);
    return QModelIndex();
}

int FilterModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    else
        return myRowCount;
}

int FilterModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    else
        return myColCount;
}

QModelIndex FilterModel::mapToSource(const QModelIndex &proxyIndex) const
{
    int r = proxyIndex.row(), c = proxyIndex.column();
    if (r < 0 || c < 0)
        return QModelIndex();

    const auto& column = myData[c];
    int originalRow = -1;
    if (r>=0 && r<column.size())
        originalRow = column[r];
    else
        originalRow = sourceModel()->rowCount()-1;
    return sourceModel()->index(originalRow, c);
}

QModelIndex FilterModel::mapFromSource(const QModelIndex &sourceIndex) const
{
    int rs = sourceIndex.row(), cs = sourceIndex.column();
    int r = myData[cs].indexOf(rs), c = cs;
    return index(r, c);
}

bool FilterModel::dropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column, const QModelIndex &parent)
{
    bool state = QAbstractProxyModel::dropMimeData(data, action, row, column, parent);
    setPattern(myPattern, mySearchInComments);
    return state;
}
