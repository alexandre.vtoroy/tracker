
set INSTALL_DIR=d:\tracker.install

del %INSTALL_DIR%
mkdir %INSTALL_DIR%

copy ..\build-tracker-Desktop_Qt_5_15_0_MinGW_64_bit-Release\release\ztracker.*.exe %INSTALL_DIR%
copy *.bat %INSTALL_DIR%
copy *.py %INSTALL_DIR%
copy ISSUES %INSTALL_DIR%
copy ..\qt\5.15.0\mingw81_64\bin\libgcc_s_seh-1.dll %INSTALL_DIR%
copy ..\qt\5.15.0\mingw81_64\bin\libstdc++-6.dll %INSTALL_DIR%
copy ..\qt\5.15.0\mingw81_64\bin\libwinpthread-1.dll %INSTALL_DIR%

..\qt\5.15.0\mingw81_64\bin\windeployqt %INSTALL_DIR%
