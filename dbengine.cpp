
#include <dbconfig.h>
#include <dbengine.h>
#include <QColor>
#include <QDateTime>
#include <QDebug>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlQueryModel>

void run(QSqlQuery& query)
{
    if (!query.exec())
        qDebug() << query.lastError().text();
}

DbEngine::DbEngine(QObject* parent)
    : QObject(parent), myDatabase(QSqlDatabase::addDatabase("QSQLITE")), myStateModel(nullptr)
{
    myDatabase.setDatabaseName(Database);
    if(!myDatabase.open())
    {
        qDebug() << myDatabase.lastError().text();
        return;
    }

    qDebug() << "Database" << Database << "is opened";

    if(!myDatabase.tables().contains(TasksTable))
        createTable(TasksTable, TasksFieldsDefinitions, 2);
    if(!myDatabase.tables().contains(StatesTable))
    {
        createTable(StatesTable, StatesFieldsDefinitions, 2);
        addState(0, "Backlog", Qt::white);
        addState(10, "For Today", Qt::cyan);
        addState(20, "In Progress", Qt::yellow);
        addState(21, "Done", Qt::green);
        addState(100, "Archived", Qt::lightGray);
    }

    myStateModel = createModel(StatesTable, StatesFields, "", "", false);
}

void DbEngine::createTable(const QString& table, const QStringList& defines, int indices)
{
    qDebug() << "Table" << table << "is not found; we create it";
    QSqlQuery query(myDatabase);
    QString queryStr = QString("CREATE TABLE %1 (%2);").arg(table).arg(defines.join(", "));
    qDebug() << queryStr;
    query.prepare(queryStr);
    run(query);

    // Create indices (on several first fields)
    QString indexStr = "CREATE INDEX idx_%1 ON %2 (%1);";
    for(int i=0; i<indices; i++)
    {
        QStringList parts = defines[i].split(" ");
        QString fieldName = parts[0];
        queryStr = indexStr.arg(fieldName).arg(table);
        qDebug() << queryStr;
        query.prepare(queryStr);
        run(query);
    }
}

QSqlQueryModel* DbEngine::createModel(const QString& table, const QStringList& fields,
                                      const QString& selector, const QString& orderBy,
                                      bool onlyNumber) const
{
    QSqlQueryModel* model = new QSqlQueryModel(const_cast<DbEngine*>(this));
    QString items = onlyNumber ? "COUNT(*)" : "*";
    QString querySelector = QString("SELECT %1 from %2").arg(items).arg(table);

    if(!selector.isEmpty())
        querySelector += " WHERE " + selector;
    if(!orderBy.isEmpty())
        querySelector += " ORDER BY " + orderBy;

    model->setQuery(querySelector, myDatabase);
    for(int i=0, n=fields.size(); i<n; i++)
        model->setHeaderData(i, Qt::Horizontal, fields[i]);
    return model;
}

QAbstractTableModel* DbEngine::stateModel() const
{
    return myStateModel;
}

QAbstractTableModel* DbEngine::createTaskModel(const QString& selector, bool onlyNumber) const
{
    return createModel(TasksTable, TasksFields, selector, "updated DESC", onlyNumber);
}


void DbEngine::resetTaskModel(QAbstractTableModel* model) const
{
    QSqlQueryModel* sqlModel = dynamic_cast<QSqlQueryModel*>(model);
    if(sqlModel)
    {
        QSqlQuery query = sqlModel->query();
        query.exec();
        sqlModel->setQuery(query);
    }
}

void DbEngine::createNewTasks(const QStringList& rawLines)
{
    QStringList descriptions;
    for(const QString& rawLine: rawLines)
    {
        QString line = rawLine.trimmed();
        if(!line.isEmpty())
        {
            descriptions.append(line);
            qDebug() << "new task:" << line;
        }
    }

    QStringList addFields = TasksFields;
    QStringList addFieldsPref = TasksFieldsPref;
    addFields.removeAt(0);
    addFieldsPref.removeAt(0);
    qDebug() << addFields;
    for(const QString& description: descriptions)
    {
        QSqlQuery query(myDatabase);
        QString queryStr = QString("INSERT INTO %1 (%2) VALUES (%3)")
                .arg(TasksTable).arg(addFields.join(", ")).arg(addFieldsPref.join(", "));
        qDebug() << queryStr;
        query.prepare(queryStr);
        query.bindValue(":description", description);
        query.bindValue(":background", Utils::randomColor());
        query.bindValue(":labels", "");
        query.bindValue(":state", 0);
        query.bindValue(":comments", "");
        query.bindValue(":updated", QDateTime::currentDateTime());
        run(query);
    }
}

void DbEngine::addState(const int id, const QString& name, const QColor& color)
{
    QSqlQuery query(myDatabase);
    QString queryStr = QString("INSERT INTO %1 (%2) VALUES (%3)")
            .arg(StatesTable).arg(StatesFields.join(", ")).arg(StatesFieldsPref.join(", "));
    query.prepare(queryStr);
    query.bindValue(":id", id);
    query.bindValue(":name", name);
    query.bindValue(":background", color);
    run(query);
}

void DbEngine::changeState(int id, int state)
{
    QSqlQuery query(myDatabase);
    QString queryStr = QString("UPDATE %1 SET state=:state, updated=:updated WHERE id=:id").arg(TasksTable);
    qDebug() << queryStr;
    query.prepare(queryStr);
    query.bindValue(":id", id);
    query.bindValue(":state", state);
    query.bindValue(":updated", QDateTime::currentDateTime());
    run(query);
}

QList<QVariant> DbEngine::taskValues(int id, QStringList& fields) const
{
    QList<QVariant> values;
    fields = TasksFields;
    QString queryStr = QString("SELECT * FROM %1 WHERE id=%2").arg(TasksTable).arg(id);
    QSqlQuery query(myDatabase);
    query.prepare(queryStr);
    run(query);
    while(query.next())
    {
        for(int i=0, n=fields.size(); i<n; i++)
            values.append(query.value(i));
        break;
    }
    return values;
}

void DbEngine::updateTask(int id, const QList<QVariant>& newValues)
{
    QSqlQuery query(myDatabase);
    QString queryStr = QString("UPDATE %1 ").arg(TasksTable);
    queryStr += "SET ";
    QStringList fields;
    for(int i=1, n=TasksFields.size(); i<n; i++)
    {
        QString field = TasksFields[i];
        fields.append(QString("%1=:%1").arg(field));
    }
    queryStr += fields.join(", ") + " WHERE id=:id";
    qDebug() << "updateTask" << id << queryStr;

    query.prepare(queryStr);
    query.bindValue(":id", id);
    query.bindValue(":description", newValues[1].toString());
    query.bindValue(":background", newValues[2].toString());
    query.bindValue(":labels", newValues[3].toString());
    query.bindValue(":state", newValues[4].toInt());
    query.bindValue(":comments", newValues[5].toString());
    query.bindValue(":updated", QDateTime::currentDateTime()); //update should always set the current date/time
    run(query);
}

QStringList DbEngine::findTasksDoneBetween(const QDateTime& start, const QDateTime& end) const
{
    QStringList result;

    QString condition = "(state=21 or state=100) and (updated>:start and updated<:end)";
    QString queryStr = QString("SELECT description FROM %1 WHERE %2").arg(TasksTable).arg(condition);
    QSqlQuery query(myDatabase);
    query.prepare(queryStr);
    query.bindValue(":start", start);
    query.bindValue(":end", end);
    run(query);
    while(query.next())
        result.append(query.value(0).toString());

    return result;
}
