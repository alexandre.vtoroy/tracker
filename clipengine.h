
#pragma once

#include <QObject>

class QTextEdit;

class ClipEngine: public QObject
{
    Q_OBJECT

public:
    ClipEngine(QTextEdit* editor, QObject* parent = nullptr);
    virtual ~ClipEngine() override = default;

    bool active() const;
    void setActive(bool value);

private slots:
    void onDataChanged();

private:
    QTextEdit* myEditor;
    bool       myActive;
    QString    myRecentClip;
};
