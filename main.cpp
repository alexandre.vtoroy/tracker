
#include <mainwindow.h>
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication aplication(argc, argv);

    MainWindow mainWindow;
    mainWindow.resize(QSize(1680, 1024));
    mainWindow.show();

    return aplication.exec();
}
