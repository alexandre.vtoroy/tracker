
#include <QStyle>
#include <QWidget>
#include <flowlayout.h>

FlowLayout::FlowLayout(QWidget *parent, int margin, int spacing)
    : QLayout(parent), mySpacing(spacing)
{
    setContentsMargins(margin, margin, margin, margin);
}

FlowLayout::~FlowLayout()
{
    QLayoutItem *item;
    while ((item = takeAt(0)))
        delete item;
}

QSize FlowLayout::sizeHint() const
{
    return QSize();
}

void FlowLayout::addItem(QLayoutItem *item)
{
    myItems.append(item);
}

QLayoutItem *FlowLayout::itemAt(int index) const
{
    return myItems.value(index);
}

QLayoutItem *FlowLayout::takeAt(int index)
{
    if (index >= 0 && index < myItems.size())
        return myItems.takeAt(index);
    return nullptr;
}

int FlowLayout::count() const
{
    return myItems.count();
}

void FlowLayout::setGeometry(const QRect& rect)
{
    QLayout::setGeometry(rect);
    doLayout(rect);
}

int FlowLayout::doLayout(const QRect &rect) const
{
    int left, top, right, bottom;
    getContentsMargins(&left, &top, &right, &bottom);
    QRect effectiveRect = rect.adjusted(+left, +top, -right, -bottom);
    int x = effectiveRect.x();
    int y = effectiveRect.y();
    int lineHeight = 0;

    for (QLayoutItem *item : qAsConst(myItems))
    {
        QWidget* widget = const_cast<QWidget*>(item->widget());

        int spaceX = mySpacing;
        if (spaceX == -1)
            spaceX = widget->style()->layoutSpacing(QSizePolicy::PushButton, QSizePolicy::PushButton, Qt::Horizontal);
        int spaceY = mySpacing;
        if (spaceY == -1)
            spaceY = widget->style()->layoutSpacing(QSizePolicy::PushButton, QSizePolicy::PushButton, Qt::Vertical);

        if (myLineBreaks.contains(widget))
        {
            x = effectiveRect.x();;
            y = y + lineHeight + spaceY;
        }

        int nextX = x + item->sizeHint().width() + spaceX;
        if (nextX - spaceX > effectiveRect.right() && lineHeight > 0) {
            x = effectiveRect.x();
            y = y + lineHeight + spaceY;
            nextX = x + item->sizeHint().width() + spaceX;
            lineHeight = 0;
        }

        item->setGeometry(QRect(QPoint(x, y), item->sizeHint()));
        x = nextX;
        lineHeight = qMax(lineHeight, item->sizeHint().height());
    }
    return y + lineHeight - rect.y() + bottom;
}

void FlowLayout::addWidget(QWidget* widget, bool withLineBreak)
{
    QLayout::addWidget(widget);
    if (withLineBreak)
        myLineBreaks.append(widget);
}
