
#pragma once

#include <QThread>

class QColor;
class QString;
class QStringList;

class Utils
{
public:
    static QColor randomColor();
    static QStringList sqlFieldsNames(const QStringList& defines);
    static QStringList addPrefix(const QStringList& items, const QString& prefix);
    static QDateTime parseDateTime(const QString&);
    static QString stdToString(const QDateTime&);

    template <typename Function> static void runAsync(Function&& func)
    {
        QThread* thr = QThread::create(func);
        QObject::connect(thr, &QThread::finished, thr, &QThread::deleteLater);
        thr->start();
    }
};
