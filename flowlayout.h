
#pragma once

#include <QLayout>

class FlowLayout : public QLayout
{
public:
    FlowLayout(QWidget* parent, int margin, int spacing);
    virtual ~FlowLayout() override;

public:
    virtual QSize sizeHint() const override;
    virtual void addItem(QLayoutItem* item) override;
    virtual QLayoutItem *itemAt(int index) const override;
    virtual QLayoutItem *takeAt(int index) override;
    virtual int count() const override;
    virtual void setGeometry(const QRect &) override;

    void addWidget(QWidget* widget, bool withLineBreak=false);

private:
    int doLayout(const QRect &rect) const;

private:
    QList<QLayoutItem*> myItems;
    int mySpacing;
    QList<QWidget*> myLineBreaks;
};
