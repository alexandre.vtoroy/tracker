
#pragma once

#include <QDockWidget>

class QCheckBox;
class QLineEdit;
class FilterModel;

class FilterPane : public QDockWidget
{
    Q_OBJECT

public:
    FilterPane(QWidget* parent = nullptr);
    virtual ~FilterPane() override = default;

    FilterModel* model() const;

public slots:
    void onSearch();

private:
    FilterModel* myModel;
    QLineEdit* myPattern;
    QCheckBox* mySearchInComments;
};
