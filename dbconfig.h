
#pragma once

#include <QStringList>
#include <utils.h>

const QString Database   = "tasks.sqlite";
const QString TasksTable = "tasks";
const QString StatesTable = "states";

const QStringList TasksFieldsDefinitions = QStringList()
        << "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT"
        << "description TEXT "
        << "background TEXT"
        << "labels TEXT"
        << "state INTEGER"
        << "comments TEXT"
        << "updated DATETIME"
           ;
const QStringList StatesFieldsDefinitions = QStringList()
        << "id INTEGER PRIMARY KEY"
        << "name TEXT "
        << "background TEXT"
           ;

const QStringList TasksFields      = Utils::sqlFieldsNames(TasksFieldsDefinitions);
const QStringList TasksFieldsPref  = Utils::addPrefix(TasksFields, ":");
const QStringList StatesFields     = Utils::sqlFieldsNames(StatesFieldsDefinitions);
const QStringList StatesFieldsPref = Utils::addPrefix(StatesFields, ":");
