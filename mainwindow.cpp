
#include <QDockWidget>
#include <QStatusBar>
#include <dbengine.h>
#include <filtermodel.h>
#include <filterpane.h>
#include <mainwindow.h>
#include <notespane.h>
#include <settings.h>
#include <snippetsboard.h>
#include <tasksmodel.h>
#include <tasksboard.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    QString version = VERSION_STRING;
    setWindowTitle(tr("Z-Wave tasks tracker %1").arg(version));
    statusBar()->showMessage(tr("ZWTT %1").arg(version));
    setFont(Settings::commonFont());

    // Main pages widget
    QTabWidget* mainPages = new QTabWidget(this);
    setCentralWidget(mainPages);

    DbEngine* dbEngine = new DbEngine(this);
    TasksModel* tasks = new TasksModel(dbEngine);

    // 1. Filter pane
    FilterPane* filtersPane = new FilterPane(this);
    FilterModel* filter = filtersPane->model();
    filter->setSourceModel(tasks);
    filtersPane->onSearch();
    addDockWidget(Qt::TopDockWidgetArea, filtersPane);

    // 2. Kanban board page
    TasksBoard* board = new TasksBoard(tasks, filter, this);
    mainPages->addTab(board, "Kanban");

    // 3. Snippets page
    SnippetsBoard* snippets = new SnippetsBoard(this);
    mainPages->addTab(snippets, "Snippets");
    connect(snippets, &SnippetsBoard::monthlyReport, tasks, &TasksModel::createMonthlyReport);

    // 4. Panes -- for notes
    NotesPane* notesPane = new NotesPane(this);
    addDockWidget(Qt::BottomDockWidgetArea, notesPane);
    connect(notesPane, &NotesPane::submitTasks, tasks, &TasksModel::createNewTasks);
}
