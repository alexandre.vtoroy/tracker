
#pragma once

#include <QDockWidget>

class QLabel;
class QPushButton;
class QTextEdit;
class QToolButton;

class NotesPane : public QDockWidget
{
    Q_OBJECT

public:
    NotesPane(QWidget* parent = nullptr);
    virtual ~NotesPane() override;

    void load();
    QString save(bool isBackup);

signals:
    void submitTasks(const QStringList&);

public slots:
    void submit();

private:
    QTextEdit*   myTextEdit;
    QLabel*      myAutoSave;
    QPushButton* mySubmit;
    bool         myNotesChanged;
    QToolButton* myGreedyClip;
};
