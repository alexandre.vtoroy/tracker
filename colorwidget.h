
#pragma once

#include <QColor>
#include <QLabel>

class ColorWidget: public QLabel
{
    Q_OBJECT

public:
    ColorWidget(QWidget* parent = nullptr);
    virtual ~ColorWidget() override = default;

    QColor color() const;
    void setColor(const QColor&);

    virtual void mouseDoubleClickEvent(QMouseEvent*) override;

private:
    QColor myColor;
};
