
#include <flowlayout.h>
#include <snippetsboard.h>
#include <QApplication>
#include <QClipboard>
#include <QLayout>
#include <QMap>
#include <QPushButton>
#include <QToolTip>
#include <QVariantAnimation>

const QMap<QString, QString> Snippets = {
    {"name", "Alexander Solovyov"},
    {"id", "11934306"},
    {"pwd", "Firstofjuly4?"},
    {"vpn", "Firstofjuly4??"},
};

const int TooltipMargin = 5;
const int FlowMargin = 20;
const int FlowSpacing = 20;
const int AnimationPeriod = 2000;
const QString BtnStyle = "QPushButton { background-color: %1; }";
const QString CopyTooltip = "Copied to clipboard";
const QColor NotClickedColor = Qt::gray;
const QColor ClickedColor    = Qt::green;

SnippetsBoard::SnippetsBoard(QWidget *parent)
    : QWidget(parent), myCurrent(nullptr)
{
    myAnimation = new QVariantAnimation(this);
    myAnimation->setStartValue(NotClickedColor);
    myAnimation->setEndValue(ClickedColor);
    myAnimation->setEasingCurve(QEasingCurve::InOutElastic);
    myAnimation->setDuration(AnimationPeriod);

    myBackAnimation = new QVariantAnimation(this);
    myBackAnimation->setStartValue(ClickedColor);
    myBackAnimation->setEndValue(NotClickedColor);
    myBackAnimation->setEasingCurve(QEasingCurve::InOutElastic);
    myBackAnimation->setDuration(AnimationPeriod);

    FlowLayout* layout = new FlowLayout(this, FlowMargin, FlowSpacing);
    for(const QString& key: Snippets.keys())
    {
        QPushButton* btn = new QPushButton(key);
        QString styleSheet = BtnStyle.arg(NotClickedColor.name());
        connect(btn, &QPushButton::clicked, this, &SnippetsBoard::onCopyWithAnimation);
        layout->addWidget(btn);
    }

    connect(myAnimation, &QVariantAnimation::valueChanged, this, &SnippetsBoard::onAnimation);
    connect(myAnimation, &QVariantAnimation::finished, [=]{
        myBackAnimation->start();
    });
    connect(myBackAnimation, &QVariantAnimation::valueChanged, this, &SnippetsBoard::onAnimation);

    QPushButton* monthly = new QPushButton("Monthly report");
    layout->addWidget(monthly, true);
    connect(monthly, &QPushButton::clicked, this, &SnippetsBoard::monthlyReport);
}

void SnippetsBoard::onCopyWithAnimation()
{
    if(myCurrent)
        myCurrent->setStyleSheet(BtnStyle.arg(NotClickedColor.name()));

    myCurrent = qobject_cast<QPushButton*>(sender());
    myAnimation->setDuration(AnimationPeriod);

    int x = myCurrent->width() + TooltipMargin;
    int y = TooltipMargin;
    QPoint p = myCurrent->mapToGlobal(QPoint(x, y));
    QToolTip::showText(p, CopyTooltip, myCurrent, QRect(), 2*AnimationPeriod);

    qApp->clipboard()->setText(Snippets[myCurrent->text()]);
    myAnimation->start();
}

void SnippetsBoard::onAnimation(const QVariant& value)
{
    if(myCurrent)
    {
        QColor current = value.value<QColor>();
        QString styleSheet = BtnStyle.arg(current.name());
        myCurrent->setStyleSheet(styleSheet);
    }
}
