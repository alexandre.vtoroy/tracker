
#include <QColorDialog>
#include <colorwidget.h>

ColorWidget::ColorWidget(QWidget* parent)
    : QLabel(parent), myColor(Qt::white)
{
}

QColor ColorWidget::color() const
{
    return myColor;
}

void ColorWidget::setColor(const QColor& color)
{
    myColor = color;
    setStyleSheet(QString("QLabel { background-color : %1; }").arg(myColor.name()));
}

void ColorWidget::mouseDoubleClickEvent(QMouseEvent*)
{
    QColorDialog colorDlg;
    colorDlg.setCurrentColor(myColor);
    if(colorDlg.exec()==QDialog::Accepted)
        setColor(colorDlg.currentColor());
}
