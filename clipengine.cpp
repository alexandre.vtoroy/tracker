
#include <QApplication>
#include <QClipboard>
#include <QDebug>
#include <QTextEdit>
#include <clipengine.h>

ClipEngine::ClipEngine(QTextEdit* editor, QObject* parent)
    : QObject(parent), myEditor(editor), myActive(false)
{
    connect(qApp->clipboard(), &QClipboard::dataChanged, this, &ClipEngine::onDataChanged);
}

bool ClipEngine::active() const
{
    return myActive;
}

void ClipEngine::setActive(bool value)
{
    myActive = value;
    myRecentClip = QString();
}

void ClipEngine::onDataChanged()
{
    if(!myActive)
        return;

    QString clip = qApp->clipboard()->text().trimmed();
    if(clip==myRecentClip)
        return;

    QString text = myEditor->toPlainText();
    text += "\n";
    text += clip + "\n";
    myEditor->setText(text);

    myRecentClip = clip;
}
