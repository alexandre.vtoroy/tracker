
#pragma once

#include <QStyledItemDelegate>

class TaskDelegate : public QStyledItemDelegate
{
public:
    TaskDelegate(QObject* parent = nullptr);
    virtual ~TaskDelegate() override = default;

protected:
    virtual void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    virtual QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const override;
};
