
#pragma once

#include <QAbstractTableModel>
#include <QList>
#include <QColor>

class DbEngine;

enum TaskRole
{
    IdRole = Qt::UserRole + 1,
    DescriptionRole,
    CommentsRole,
};

class TasksModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    TasksModel(DbEngine*);
    virtual ~TasksModel() override = default;

    virtual int rowCount(const QModelIndex &parent) const override;
    virtual int columnCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    virtual Qt::ItemFlags flags(const QModelIndex &index) const override;

    virtual QStringList mimeTypes() const override;
    virtual QMimeData *mimeData(const QModelIndexList &indices) const override;
    virtual bool canDropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) const override;
    virtual bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) override;
    virtual Qt::DropActions supportedDropActions() const override;

    QList<QVariant> values(const QModelIndex& index, QStringList& fields) const;
    void            setValues(const QModelIndex& index, const QList<QVariant>& newValues);

    QList<int> states() const;
    QStringList statesNames() const;

public slots:
    void createNewTasks(const QStringList& rawLines);
    void createMonthlyReport();

private:
    DbEngine* myEngine;
    QList<int> myStateId;
    QStringList myStateName;
    QList<QColor> myStateColor;
    QList<QAbstractTableModel*> myStateModel;
};
