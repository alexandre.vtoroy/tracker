
#pragma once

#include <QDialog>

class TasksModel;

class TaskEditDlg : public QDialog
{
    Q_OBJECT

public:
    TaskEditDlg(TasksModel* tasks, QWidget* parent = nullptr);
    virtual ~TaskEditDlg() override = default;

    void buildLayout(const QStringList& fields, const QList<QVariant>& values);
    QWidget* createWidget(const QString& field, const QVariant& value) const;

    QList<QVariant> currentValues() const;
    QVariant currentValue(const QString& field, QWidget* widget) const;

private:
    TasksModel* myTasks;
    QStringList myFields;
    QList<QWidget*> myWidgets;
};
