
#pragma once

#include <QAbstractProxyModel>

class FilterModel : public QAbstractProxyModel
{
public:
    FilterModel(QObject* parent = nullptr);
    virtual ~FilterModel() = default;

    void setPattern(const QString& pattern, bool searchInComments);

    virtual QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    virtual QModelIndex parent(const QModelIndex &child) const override;
    virtual int rowCount(const QModelIndex &parent) const override;
    virtual int columnCount(const QModelIndex &parent) const override;
    virtual QModelIndex mapToSource(const QModelIndex &proxyIndex) const override;
    virtual QModelIndex mapFromSource(const QModelIndex &sourceIndex) const override;
    virtual bool dropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column, const QModelIndex &parent) override;

protected:
    bool filterAccept(const QModelIndex& source) const;

private:
    QString myPattern;
    bool mySearchInComments = false;
    int myRowCount = 0;
    int myColCount = 0;

    typedef QList<int> ColumnData;
    typedef QList<ColumnData> Data;
    Data myData;
};
