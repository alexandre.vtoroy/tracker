
#include <filtermodel.h>
#include <settings.h>
#include <taskdelegate.h>
#include <taskeditdlg.h>
#include <tasksboard.h>
#include <tasksheaderview.h>
#include <tasksmodel.h>

TasksBoard::TasksBoard(TasksModel* tasksModel, FilterModel* filterModel, QWidget* parent)
    : QTableView(parent), myFilter(filterModel), myTasks(tasksModel)
{
    setModel(myFilter);
    setDragEnabled(true);
    setDragDropMode(QAbstractItemView::DragDrop);
    setShowGrid(true);
    setHorizontalHeader(new TasksHeaderView(this));
    horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    setFont(Settings::commonFont());
    setItemDelegate(new TaskDelegate(this));

    connect(this, &QTableView::doubleClicked, this, &TasksBoard::onEdit);
}

void TasksBoard::onEdit(const QModelIndex& proxyIndex) const
{
    QModelIndex index = myFilter->mapToSource(proxyIndex);

    QStringList fields;
    QList<QVariant> values = myTasks->values(index, fields);
    if(values.isEmpty())
        return;

    //qDebug() << "Fields:" << fields;
    //qDebug() << "Values:" << values;
    TaskEditDlg editDlg(myTasks, const_cast<TasksBoard*>(this));
    editDlg.buildLayout(fields, values);
    editDlg.resize(640, 480);
    if(editDlg.exec()==QDialog::Accepted)
    {
        QList<QVariant> newValues = editDlg.currentValues();
        //qDebug() << "New values:" << newValues;
        myTasks->setValues(index, newValues);
    }
}
