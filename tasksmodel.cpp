
#include <tasksmodel.h>
#include <dbengine.h>
#include <QColor>
#include <QDataStream>
#include <QDateTime>
#include <QDebug>
#include <QFile>
#include <QMimeData>
#include <QMessageBox>

const QString TrackerIdsMime = "tracker/ids";
const QMap<QString, QString> Labels = {
    {"app", "🧬"},
    {"blocked", "⛔"},
    {"fo", "👔"},
    {"know", "📚"},
    {"igc", "⚡"},
    {"rec", "☔"},
};

const int MonthPeriodStartDay = 10;
const int AverageMonthLength = 30;


TasksModel::TasksModel(DbEngine* engine)
    : QAbstractTableModel(engine), myEngine(engine)
{
    QAbstractTableModel* states = myEngine->stateModel();
    int n = states->rowCount();
    for(int i=0; i<n; i++)
    {
        int state = states->index(i, 0).data().toInt();
        QString name = states->index(i, 1).data().toString();
        QColor color = states->index(i, 2).data().toString();
        myStateId.append(state);
        myStateName.append(name);
        myStateColor.append(color);
        QString selector = QString("state = %1").arg(state);
        bool onlyNumber = (name=="Archived");
        myStateModel.append(myEngine->createTaskModel(selector, onlyNumber));
    }
}

int TasksModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    int n = 0;
    for(const auto& model: myStateModel)
    {
        int rc = model->rowCount();
        if(rc > n)
            n = rc;
    }
    n++;
    return n;
}

int TasksModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return myStateModel.count();
}

QVariant TasksModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(orientation==Qt::Horizontal)
    {
        if(role==Qt::DisplayRole)
            return myStateName[section];
        else if(role==Qt::BackgroundRole)
            return myStateColor[section];
    }
    return QVariant();
}

Qt::ItemFlags TasksModel::flags(const QModelIndex &index) const
{
    Q_UNUSED(index);
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled;
}

QVariant TasksModel::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    int col = index.column();
    if(col < 0 || col >= myStateModel.count())
        return QVariant();

    QAbstractTableModel* model = myStateModel[col];
    QVariant id = model->index(row, 0).data();
    QVariant description = model->index(row, 1).data();
    QVariant background = model->index(row, 2).data();
    QVariant labels = model->index(row, 3).data();
    QVariant state = model->index(row, 4).data();
    QVariant comments = model->index(row, 5).data();

    if(!id.isValid())
        return QVariant();

    switch(role)
    {
    case IdRole:
        return id;
    case DescriptionRole:
        return description;
    case CommentsRole:
        return comments;

    //case Qt::ToolTipRole:
    case Qt::DisplayRole:
        {
            if (id.isValid() && !description.isValid() && !background.isValid())
            {
                return QString("<span style=\"font-size: 24px;\">%1 tasks</span>").arg(id.toInt());
            }

            QString descText = QString("<span style=\"font-size: 14px;\">%1. %2</span>").arg(id.toInt()).arg(description.toString());
            QString labelsText = labels.isValid() ? labels.toString() : QString();
            if(!labelsText.isEmpty())
            {
                QStringList parts = labelsText.split(", ");
                for(int i=0, n=parts.size(); i<n; i++)
                {
                    auto it = Labels.find(parts[i]);
                    if(it==Labels.end())
                        parts[i] = "[" + parts[i] + "]";
                    else
                        parts[i] = it.value();
                }
                labelsText = parts.join("");
            }
            labelsText = QString("<br><span style=\"font-size: 24px;\">%1</span>").arg(labelsText);
            return descText + labelsText;
        }
    case Qt::BackgroundRole:
        if(background.isValid())
            return QColor(background.toString());
    }

    return QVariant();
}

QStringList TasksModel::mimeTypes() const
{
    static QStringList types = QStringList() << "tracker/ids";
    return types;
}

QMimeData* TasksModel::mimeData(const QModelIndexList &indices) const
{
    QMimeData *mimeData = new QMimeData();
    QByteArray encodedData;
    QDataStream stream(&encodedData, QIODevice::WriteOnly);

    QList<int> ids;
    for(auto index: indices)
    {
        int id = index.data(IdRole).toInt();
        ids.append(id);
    }

    stream << ids.length();
    for(auto id: ids)
        stream << id;
    mimeData->setData(TrackerIdsMime, encodedData);
    return mimeData;
}

bool TasksModel::canDropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) const
{
    Q_UNUSED(action);
    Q_UNUSED(row);
    Q_UNUSED(column);
    Q_UNUSED(parent);
    return (data->hasFormat(TrackerIdsMime));
}

Qt::DropActions TasksModel::supportedDropActions() const
{
    return Qt::MoveAction;
}

bool TasksModel::dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent)
{
    Q_UNUSED(action);
    Q_UNUSED(row);
    Q_UNUSED(column);
    if (data->hasFormat(TrackerIdsMime))
    {
        QByteArray encodedData = data->data(TrackerIdsMime);
        QDataStream stream(&encodedData, QIODevice::ReadOnly);

        int n; stream >> n;
        int newStateIndex = myStateId[parent.column()];
        beginResetModel();
        for(int i=0; i<n; i++)
        {
            int id; stream >> id;
            myEngine->changeState(id, newStateIndex);
        }
        for(QAbstractTableModel* model: myStateModel)
            myEngine->resetTaskModel(model);
        endResetModel();
    }
    return true;
}

void TasksModel::createNewTasks(const QStringList& rawLines)
{
    beginResetModel();
    myEngine->createNewTasks(rawLines);
    for(QAbstractTableModel* model: myStateModel)
        myEngine->resetTaskModel(model);
    endResetModel();
}

QList<QVariant> TasksModel::values(const QModelIndex& index, QStringList& fields) const
{
    int row = index.row();
    int col = index.column();
    QAbstractTableModel* model = myStateModel[col];
    int id = model->index(row, 0).data().toInt();
    return myEngine->taskValues(id, fields);
}

void TasksModel::setValues(const QModelIndex& index, const QList<QVariant>& newValues)
{
    int row = index.row();
    int col = index.column();
    QAbstractTableModel* model = myStateModel[col];
    int id = model->index(row, 0).data().toInt();
    beginResetModel();
    myEngine->updateTask(id, newValues);
    for(QAbstractTableModel* model: myStateModel)
        myEngine->resetTaskModel(model);
    endResetModel();
}

QList<int> TasksModel::states() const
{
    return myStateId;
}

QStringList TasksModel::statesNames() const
{
    return myStateName;
}

void TasksModel::createMonthlyReport()
{
    qDebug() << "createMonthlyReport";

    QDateTime current = QDateTime::currentDateTime();

    QDateTime end = QDate(current.date().year(), current.date().month(), MonthPeriodStartDay).startOfDay();
    QDateTime start = end.addDays( - AverageMonthLength);
    start = QDate(start.date().year(), start.date().month(), MonthPeriodStartDay).startOfDay();

    QString fileName = tr("monthly_report_") + current.toString("d_MM_yyyy") + ".txt";
    QStringList descriptions = myEngine->findTasksDoneBetween(start, current);
    descriptions.append(""); //add empty line

    QFile file(fileName);
    if(file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&file);
        stream.setCodec("UTF-8");
        stream << descriptions.join("\n");
        stream.flush();
        file.close();
    }
    QMessageBox::information(qobject_cast<QWidget*>(sender()), tr("Monthly report"), tr("Monthly report is written to %1").arg(fileName));
}
