
#include <tasksheaderview.h>
#include <QPainter>

TasksHeaderView::TasksHeaderView(QWidget* parent)
    : QHeaderView(Qt::Horizontal, parent)
{
}

void TasksHeaderView::paintSection(QPainter* painter, const QRect &rect, int logicalIndex) const
{
    painter->setRenderHint(QPainter::Antialiasing, true);
    painter->setRenderHint(QPainter::TextAntialiasing, true);

    QString name = model()->headerData(logicalIndex, orientation(), Qt::DisplayRole).toString();
    QColor background = model()->headerData(logicalIndex, orientation(), Qt::BackgroundRole).value<QColor>();
    painter->save();
    painter->setBrush(background);
    const int d = 3;
    QRectF rectBound = rect.adjusted(d, d, -d, -d);
    painter->drawRoundedRect(rectBound, 3*d, 3*d);
    painter->restore();
    painter->drawText(rect, Qt::AlignHCenter | Qt::AlignVCenter, name);
}
