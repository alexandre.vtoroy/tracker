
#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFileSystemWatcher>
#include <QGridLayout>
#include <QLabel>
#include <QMessageBox>
#include <QPushButton>
#include <QTextEdit>
#include <QThread>
#include <QTimer>
#include <QToolButton>
#include <clipengine.h>
#include <notespane.h>
#include <settings.h>
#include <utils.h>

const QString NotesFile = "notes%1.txt";
const int     AutosavePeriod = 20000;
const int     PreloadPeriod  = 250;

NotesPane::NotesPane(QWidget* parent)
    : QDockWidget(parent), myNotesChanged(false)
{
    QTimer* timer = new QTimer();
    connect(timer, &QTimer::timeout, [=]{
        bool toSave = this->myNotesChanged && !myAutoSave->isVisible();
        myAutoSave->setVisible(toSave);
        if (toSave)
            Utils::runAsync([=]{ this->save(false); });
    });
    timer->start(AutosavePeriod / 2);

    QString notesFile = QDir::current().path() + "/" + NotesFile.arg("");
    QFileSystemWatcher* watcher = new QFileSystemWatcher(this);
    watcher->addPath(notesFile);
    connect(watcher, &QFileSystemWatcher::fileChanged, [=] {
        QThread::msleep(PreloadPeriod);
        this->load();
    });

    setWindowTitle(tr("Notes tracker"));

    QWidget* notes = new QWidget(this);
    setWidget(notes);

    QGridLayout* layout = new QGridLayout(notes);
    myAutoSave = new QLabel(this);
    myAutoSave->setText("\U0001f4be Autosave...");
    myAutoSave->setFont(Settings::commonFont());
    myAutoSave->setVisible(false);

    mySubmit = new QPushButton(this);
    mySubmit->setText("Submit");
    mySubmit->setFont(Settings::commonFont());
    mySubmit->setToolTip("Automatically convert notes to tasks: each non-empty line will be a new task.");
    connect(mySubmit, &QPushButton::clicked, this, &NotesPane::submit);

    myTextEdit = new QTextEdit(this);
    myTextEdit->setAcceptRichText(false);
    myTextEdit->setFont(Settings::commonFont());
    connect(myTextEdit, &QTextEdit::textChanged, [=]{ this->myNotesChanged = true; });

    QVBoxLayout* btnsLayout = new QVBoxLayout(this);
    layout->addWidget(myTextEdit, 0, 0);
    layout->addLayout(btnsLayout, 0, 1);

    ClipEngine* clipEngine = new ClipEngine(myTextEdit, this);
    clipEngine->setActive(false);

    myGreedyClip = new QToolButton(this);
    myGreedyClip->setText("\U0001f355 Greedy Clip");
    myGreedyClip->setFont(Settings::commonFont());
    myGreedyClip->setCheckable(true);
    myGreedyClip->setToolTip("Automatically insert data from clipboard as the notes for further tasks creation.");
    connect(myGreedyClip, &QToolButton::clicked, [=]{
       clipEngine->setActive(myGreedyClip->isChecked());
    });

    btnsLayout->addWidget(myGreedyClip);
    btnsLayout->addWidget(myAutoSave);
    btnsLayout->addStretch(1);
    btnsLayout->addWidget(mySubmit);

    layout->setColumnStretch(0, 1);
    layout->setRowStretch(0, 1);
    layout->setColumnMinimumWidth(1, 150);

    load();
}

NotesPane::~NotesPane()
{
    save(false);
}

void NotesPane::load()
{
    QFile file(NotesFile.arg(""));
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    QTextStream stream(&file);
    stream.setCodec("UTF-8");
    QString notes = stream.readAll();
    if(notes.trimmed() != myTextEdit->toPlainText().trimmed())
        myTextEdit->setText(notes);
    file.close();
    myNotesChanged = false;
}

QString NotesPane::save(bool isBackup)
{
    QString suffix;
    if(isBackup)
        suffix = QDateTime::currentDateTime().toString(".yyyy_MM_dd__hh_mm");

    QString fileName = NotesFile.arg(suffix);
    QFile file(fileName);
    if(file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&file);
        stream.setCodec("UTF-8");
        stream << myTextEdit->toPlainText();
        stream.flush();
        file.close();
    }
    myNotesChanged = false;
    return fileName;
}

void NotesPane::submit()
{
    QString title = "Submit tasks";
    if(QMessageBox::question(this, title, "Are you ready to submit tasks?")==QMessageBox::Yes)
    {
        QString fileName = save(true);
        QString msg = "Tasks are submitted, the backup of notes is %1";
        msg = msg.arg(fileName);
        QMessageBox::information(this, title, msg);

        QStringList lines = myTextEdit->toPlainText().split("\n");
        emit submitTasks(lines);

        myTextEdit->setText("");
        myGreedyClip->setChecked(false);
    }
}
