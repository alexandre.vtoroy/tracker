
#pragma once

#include <QHeaderView>

class TasksHeaderView: public QHeaderView
{
public:
    TasksHeaderView(QWidget* parent = nullptr);
    virtual ~TasksHeaderView() override = default;

protected:
    virtual void paintSection(QPainter* painter, const QRect &rect, int logicalIndex) const override;
};
